const express = require('express');
const User = require('../models/User');
const router = express.Router();

router.post('/', async (req, res) => {

    if(!req.body.username || !req.body.password) {
        return res.status(400).send({error: 'Username or password key was not entered'});
    }

    const userData = {
        username: req.body.username,
        password: req.body.password,
    };

    const user = new User(userData);

    try {
        user.generateToken();
        await user.save();
        res.send(user);
    }
    catch (e) {
        res.sendStatus(500);
    }
});

router.post('/sessions', async (req,res) => {
   const user = await User.findOne({username: req.body.username});
   if (!user) {
       return res.status(401).send({error: 'Username not found'});
   }

   const isMathch = await user.checkPassword(req.body.password);
   if(!isMathch) {
        return res.status(401).send({error: 'Password is wrong'});
   }

   try {
       user.generateToken();
       await user.save();
       res.send({message: 'Username and password correct!', user});
   }
   catch (e) {
       res.sendStatus(500);
   }
});

module.exports = router;