const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');
const router = express.Router();

router.post('/', auth, async (req, res) => {
    if (!req.body.title) {
        return res.status(400).send({error: 'No name for task presented. Please, enter title of task!'});
    }
    if (!req.body.status) {
        return res.status(400).send({error: 'No status for task presented. Please, enter status of task!'});
    }

    const taskData = {
        title: req.body.title,
        status: req.body.status,
        description: req.body.description || null,
        user: req.user,
    };

    const task = new Task(taskData);

    try {
        await task.save();
        res.send({message: 'Task created', task});
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/', auth, async (req, res) => {
    const tasks = await Task.find({user: req.user});

    if (tasks.length === 0) {
        return res.send({message: 'No tasks found for current user'});
    }
    res.send({message: 'Tasks found !', tasks});
});

router.put('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOne({
            "_id": req.params.id,
            "user": req.user
        });

        if (!task) {
            res.status(400).send({message: "Task was not found for such user"});
        }
        else {
            const updatedTask = await Task.updateOne(task, {
                title: req.body.title || task.title,
                status: req.body.status || task.status,
                description: req.body.description || task.description
            });

            res.send({message: 'Task successfully updated', updatedTask});
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.delete('/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOne({
            "_id": req.params.id,
            "user": req.user
        });

        if (!task) {
            res.status(400).send({message: "Task was not found for such user"});
        }
        else {
            const deletedTask = await Task.findByIdAndDelete(req.params.id)
            if (deletedTask) {
                res.send({message: `Task '${deletedTask.title}' removed`});
            } else {
                res.send({message: `Task was not found`});
            }
        }
    }
    catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;