const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
   title: {
      type: String,
      required: true
   },
   user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
   },
   description: String,
   status: {
      type: String,
      enum: ['new', 'in_progress', 'complete'],
      required: true
   },
});

const Task = new mongoose.model('Task', TaskSchema);

module.exports = Task;